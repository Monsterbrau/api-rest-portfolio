<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use App\Repository\ProjectRepository;
use Symfony\Component\BrowserKit\Response;
use App\Entity\Project;
use Symfony\Component\HttpFoundation\Request;

class ProjectController extends Controller
{
    private $serializer;

    public function __construct()
    {
        $encoder = new JsonEncoder();
        $normalizer = new ObjectNormalizer();
        $normalizer->setCircularReferenceLimit(1);
        $normalizer->setCircularReferenceHandler(function ($object) {
            return $object->getId();
        });
        $this->serializer = new Serializer([$normalizer], [$encoder]);
    }

    /**
     * @Route("/project", name="all_project", methods={"GET"})
     */

    public function all(ProjectRepository $repo)
    {
        $list = $repo->findAll();

        $data = $this->serializer->normalize($list, null, ['attributes' => ['id', 'name']]);

        $response = new Response($this->serializer->serialize($data, "json"));
        return $response;
    }

     /**
     * @Route("/project/{project}", name="one_project", methods={"GET"})
     */

    public function one(Project $project)
    {
        $data = $this->serializer->normalize($productList, null, ['attributes' => ['id', 'name', 'products' => ['id', 'name', 'tag', 'qty', 'done']]]);

        $response = new Response($this->serializer->serialize($data, "json"));
        return $response;
    }

    /**
     * @Route("/project", name="new_project", methods={"POST"})
     */

    public function addProject(Request $request, Project $project)
    {
        $manager = $this->getDoctrine()->getManager();

        $content = $request->getContent();
        $project = $this->serializer->deserialize($content, Project::class, "json");

        $data = $this->serializer->normalize($project, null, ['attributes' => ['id', 'name', 'description', 'link', 'git','img']]);        

        $response = new Response($this->serializer->serialize($project, "json"));
        return $response;
    }

    /**
     * @Route("/{project}", name="updade_project", methods={"PUT"})
     */

    public function update(Request $request, Project $project)
    {
        $manager = $this->getDoctrine()->getManager();

        $content = $request->getContent();
        $update = $this->serializer->deserialize($content, Project::class, "json");

        $project->setName($update->getName());

        $manager->persist($project);
        $manager->flush();

        $data = $this->serializer->normalize($project, null, ['attributes' => ['id', 'name']]);        

        $response = new Response($this->serializer->serialize($project, "json"));
        return $response;
    }

    /**
     * @Route("/{project}", name="delete_project", methods={"DELETE"})
     */

    public function delete(Project $project)
    {
        $manager = $this->getDoctrine()->getManager();

        $manager->remove($project);
        $manager->flush();

        return new Response("OK", 204);
    }


}
